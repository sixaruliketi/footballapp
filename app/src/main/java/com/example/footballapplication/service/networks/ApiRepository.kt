package com.example.footballapplication.service.networks

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiRepository {

    private const val BASE_URL = "https://run.mocky.io/"

    fun ApiRepository(): MockyApi{
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(MockyApi::class.java)
    }

}