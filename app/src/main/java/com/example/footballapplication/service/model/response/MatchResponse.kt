package com.example.footballapplication.service.model.response

import com.example.footballapplication.service.model.Match

data class MatchResponse(val resultCode: Int? = 0,
                         val match: Match? = null
)