package com.example.footballapplication.service.model

data class Player(
    val playerName: String? = null,
    val playerImage: String? = null
)