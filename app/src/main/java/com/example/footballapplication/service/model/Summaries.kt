package com.example.footballapplication.service.model

data class Summaries(
        val actionTime: Int? = null,
        val team1Action: MutableList<TeamAction>? = arrayListOf(),
        val team2Action: MutableList<TeamAction>? = arrayListOf()
)