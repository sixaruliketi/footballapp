package com.example.footballapplication.service.model

data class TeamAction(
    val actionType: Int? = null,
    val teamType: String? = null,
    val action: Action? = null
)