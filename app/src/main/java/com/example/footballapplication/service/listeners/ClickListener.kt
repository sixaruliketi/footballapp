package com.example.footballapplication.service.listeners

interface ClickListener {
    fun itemClickListener(position: Int)
}