package com.example.footballapplication.service.enums

enum class MatchTeamType(val value: Int) {
    TEAM1(1),
    TEAM2(2)
}