package com.example.footballapplication.service.model

import com.google.gson.annotations.SerializedName

data class Action(
        val goalType: Int? = null,
        @SerializedName("player", alternate = ["player1"])
        val player1: Player? = null,
        val player2: Player? = null
)