package com.example.footballapplication.service.networks

import com.example.footballapplication.service.model.response.MatchResponse
import retrofit2.Response
import retrofit2.http.GET

interface MockyApi {

    @GET("v3/48bb936e-261a-4471-a362-3bbb3b9a2a58")
    suspend fun getAllItems(): Response<MatchResponse>

}