package com.example.footballapplication.service.model

data class MatchSummary(
    val summaries: MutableList<Summaries>? = null
)