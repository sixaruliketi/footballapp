package com.example.footballapplication.ui.matches.adapters

import android.annotation.SuppressLint
import android.opengl.Visibility
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.footballapplication.R
import com.example.footballapplication.databinding.RecyclerViewTeamOneItemViewBinding
import com.example.footballapplication.service.enums.GoalType
import com.example.footballapplication.service.enums.MatchActionType
import com.example.footballapplication.service.model.Summaries

class MatchRecyclerViewTeamOne (private val summaries: Summaries):
    RecyclerView.Adapter<MatchRecyclerViewTeamOne.ItemViewHolder>(){

    inner class ItemViewHolder(private val binding: RecyclerViewTeamOneItemViewBinding):
        RecyclerView.ViewHolder(binding.root){
        @SuppressLint("SetTextI18n")
        fun bind(){

            for (team1Action in summaries.team1Action?: arrayListOf()){
                when(team1Action.actionType){
                    MatchActionType.GOAL.value -> {
                        binding.itemOneGoalType.text = "goals by"
                        binding.itemOnePlayerName.text = summaries.team1Action?.get(adapterPosition)?.action?.player1?.playerName
                        Glide.with(binding.itemOnePlayerImage.context).load(team1Action.action?.player1?.playerImage).into(binding.itemOnePlayerImage)
                        when(team1Action.action?.goalType){
                            GoalType.GOAL.value -> binding.itemOneActionType.setImageResource(R.drawable.ic_green_ball)
                            GoalType.OWN_GOAL.value -> binding.itemOneActionType.setImageResource(R.drawable.ic_red_ball)
                        }
                    }
                    MatchActionType.YELLOW_CARD.value -> {
                        binding.itemOneGoalType.text = "yellow card"
                        binding.itemOnePlayerName.text = team1Action.action?.player1?.playerName
                        Glide.with(binding.itemOnePlayerImage.context).load(team1Action.action?.player1?.playerImage).into(binding.itemOnePlayerImage)
                        binding.itemOneActionType.setImageResource(R.drawable.ic_yellow_card)
                    }
                    MatchActionType.RED_CARD.value -> {
                        binding.itemOneGoalType.text = "red card"
                        binding.itemOnePlayerName.text = team1Action.action?.player1?.playerName
                        Glide.with(binding.itemOnePlayerImage.context).load(team1Action.action?.player1?.playerImage).into(binding.itemOnePlayerImage)
                        binding.itemOneActionType.setImageResource(R.mipmap.ic_card_red)
                    }
                    MatchActionType.SUBSTITUTION.value -> {
                        binding.itemOneGoalType.text = "substitution"
                        binding.itemOnePlayerName.text = team1Action.action?.player1?.playerName
                        binding.itemTwoPlayerName.isVisible = true
                        Glide.with(binding.itemOnePlayerImage.context).load(team1Action.action?.player1?.playerImage).into(binding.itemOnePlayerImage)
                        binding.itemTwoPlayerName.text = team1Action.action?.player2?.playerName
                        binding.itemTwoPlayerImage.isVisible = true
                        Glide.with(binding.itemTwoPlayerImage.context).load(team1Action.action?.player2?.playerImage).into(binding.itemTwoPlayerImage)
                        binding.itemOneActionType.setImageResource(R.mipmap.ic_substitution)
                    }
                }
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchRecyclerViewTeamOne.ItemViewHolder {
        return ItemViewHolder(
            RecyclerViewTeamOneItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    }
    override fun onBindViewHolder(holder: MatchRecyclerViewTeamOne.ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = summaries.team1Action?.size ?: 0

}