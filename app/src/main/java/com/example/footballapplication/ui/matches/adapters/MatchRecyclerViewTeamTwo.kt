package com.example.footballapplication.ui.matches.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.footballapplication.R
import com.example.footballapplication.databinding.RecyclerViewTeamTwoItemViewBinding
import com.example.footballapplication.service.enums.GoalType
import com.example.footballapplication.service.enums.MatchActionType
import com.example.footballapplication.service.model.Summaries

class MatchRecyclerViewTeamTwo (private val summaries: Summaries):
    RecyclerView.Adapter<MatchRecyclerViewTeamTwo.ItemViewHolder>(){

    inner class ItemViewHolder(private val binding: RecyclerViewTeamTwoItemViewBinding):
        RecyclerView.ViewHolder(binding.root){
            @SuppressLint("SetTextI18n")
            fun bind(){

                for (team2Action in summaries.team2Action?: arrayListOf()){
                    when(team2Action.actionType){
                        MatchActionType.GOAL.value -> {
                            binding.itemTwoGoalType.text = "goals by"
                            binding.itemOnePlayerName.text = summaries.team2Action?.get(adapterPosition)?.action?.player1?.playerName
                            Glide.with(binding.itemOnePlayerImage.context).load(team2Action.action?.player1?.playerImage).into(binding.itemOnePlayerImage)
                            when(team2Action.action?.goalType){
                                GoalType.GOAL.value -> binding.itemTwoActionType.setImageResource(R.drawable.ic_green_ball)
                                GoalType.OWN_GOAL.value -> binding.itemTwoActionType.setImageResource(
                                    R.drawable.ic_red_ball)
                            }
                        }
                        MatchActionType.YELLOW_CARD.value -> {
                            binding.itemTwoGoalType.text = "yellow card"
                            binding.itemOnePlayerName.text = team2Action.action?.player1?.playerName
                            Glide.with(binding.itemOnePlayerImage.context).load(team2Action.action?.player1?.playerImage).into(binding.itemOnePlayerImage)
                            binding.itemTwoActionType.setImageResource(R.drawable.ic_yellow_card)
                        }
                        MatchActionType.RED_CARD.value -> {
                            binding.itemTwoGoalType.text = "red card"
                            binding.itemOnePlayerName.text = team2Action.action?.player1?.playerName
                            Glide.with(binding.itemOnePlayerImage.context).load(team2Action.action?.player1?.playerImage).into(binding.itemOnePlayerImage)
                            binding.itemTwoActionType.setImageResource(R.mipmap.ic_card_red)
                        }
                        MatchActionType.SUBSTITUTION.value -> {
                            binding.itemTwoGoalType.text = "substitution"
                            binding.itemOnePlayerName.text = team2Action.action?.player1?.playerName
                            binding.itemTwoPlayerName.isVisible = true
                            binding.itemTwoPlayerName.text = team2Action.action?.player2?.playerName
                            Glide.with(binding.itemOnePlayerImage.context).load(team2Action.action?.player1?.playerImage).into(binding.itemOnePlayerImage)
                            binding.itemTwoPlayerImage.isVisible = true
                            Glide.with(binding.itemTwoPlayerImage.context).load(team2Action.action?.player2?.playerImage).into(binding.itemTwoPlayerImage)
                            binding.itemTwoActionType.setImageResource(R.mipmap.ic_substitution)
                        }
                    }
                }
            }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchRecyclerViewTeamTwo.ItemViewHolder {
        return ItemViewHolder(
            RecyclerViewTeamTwoItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    }
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = summaries.team2Action?.size ?: 0

}