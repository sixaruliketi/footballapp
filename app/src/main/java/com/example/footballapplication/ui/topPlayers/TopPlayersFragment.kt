package com.example.footballapplication.ui.topPlayers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.footballapplication.databinding.FragmentTopPlayersBinding

class TopPlayersFragment : Fragment() {

    private lateinit var binding: FragmentTopPlayersBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTopPlayersBinding.inflate(inflater, container, false)

        return binding.root
    }

}