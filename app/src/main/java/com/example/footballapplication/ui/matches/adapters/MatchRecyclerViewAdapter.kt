package com.example.footballapplication.ui.matches.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.footballapplication.databinding.RecyclerViewRecyclerItemsBinding
import com.example.footballapplication.service.model.Summaries

class MatchRecyclerViewAdapter:
    RecyclerView.Adapter<MatchRecyclerViewAdapter.ItemViewHolder>(){

    private var summaries = mutableListOf<Summaries>()
    private lateinit var adapterItemOne: MatchRecyclerViewTeamOne
    private lateinit var adapterItemTwo: MatchRecyclerViewTeamTwo

    inner class ItemViewHolder(private val binding: RecyclerViewRecyclerItemsBinding):
        RecyclerView.ViewHolder(binding.root){
            fun bind(){
                initRecyclerOne()
                initRecyclerTwo()
            }
            private fun initRecyclerOne(){
                adapterItemOne = MatchRecyclerViewTeamOne(summaries[adapterPosition])
                binding.matchRecyclerViewTeamOne.layoutManager = LinearLayoutManager(binding.root.context)
                binding.matchRecyclerViewTeamOne.adapter = adapterItemOne
            }
            private fun initRecyclerTwo(){
                adapterItemTwo = MatchRecyclerViewTeamTwo(summaries[adapterPosition])
                binding.matchRecyclerViewTeamTwo.layoutManager = LinearLayoutManager(binding.root.context)
                binding.matchRecyclerViewTeamTwo.adapter = adapterItemTwo
            }
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            RecyclerViewRecyclerItemsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }


    override fun getItemCount(): Int = summaries.size

    fun getData(summaries: List<Summaries>){
        this.summaries = summaries as MutableList<Summaries>
        notifyDataSetChanged()
    }

}