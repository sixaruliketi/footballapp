package com.example.footballapplication.ui.matches

import android.util.Log.d
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.footballapplication.service.model.response.MatchResponse
import com.example.footballapplication.service.networks.ApiRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MatchesViewModel : ViewModel() {

    val _liveData = MutableLiveData<MatchResponse>().apply {
        mutableListOf<MatchResponse>()
    }
    val liveData: MutableLiveData<MatchResponse> = _liveData


    fun init(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                getItems()
            }
        }

    }

    suspend fun getItems(){

        val getItems = ApiRepository.ApiRepository().getAllItems()
        if (getItems.isSuccessful){
            val items = getItems.body()
            d("msg","$items")
            liveData.postValue(items)
        }else {
            getItems.code()
        }


    }
}