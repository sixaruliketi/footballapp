package com.example.footballapplication.ui.matches

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.footballapplication.databinding.FragmentMatchesBinding
import com.example.footballapplication.ui.matches.adapters.MatchRecyclerViewAdapter

class MatchesFragment : Fragment() {

    private val matchesViewModel: MatchesViewModel by viewModels()
    private lateinit var binding: FragmentMatchesBinding
    private lateinit var adapter: MatchRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMatchesBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init(){
        matchesViewModel.init()
        adapter = MatchRecyclerViewAdapter()
        binding.matchRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.matchRecyclerView.adapter = adapter
        observer()
    }

    @SuppressLint("SetTextI18n")
    private fun observer(){
        matchesViewModel._liveData.observe(viewLifecycleOwner, {
            val ballPossession = it.match?.team1?.ballPosition
            if (ballPossession != null) {
                binding.progressBar.progress = ballPossession.toInt()
            }
            binding.matchesStadiumAdress.text = it.match?.stadiumAdress
            binding.matchCurrentScore.text = "${it.match?.team1?.score} : ${it.match?.team2?.score}"
            binding.matchCurrentHalfScore.text = "${it.match?.team1?.score} : ${it.match?.team2?.score}"
            binding.matchesMatchDate.text = it.match?.matchDate.toString()
            Glide.with(binding.teamOneImage.context).load(it.match?.team1?.teamImage).into(binding.teamOneImage)
            Glide.with(binding.teamTwoImage.context).load(it.match?.team2?.teamImage).into(binding.teamTwoImage)
            binding.matchCurrentTime.text = it.match?.matchTime.toString()
            binding.teamOneName.text = it.match?.team1?.teamName
            binding.teamTwoName.text = it.match?.team2?.teamName

            it.match?.matchSummary?.summaries?.let { it1 -> adapter.getData(it1) }
        })
    }
}